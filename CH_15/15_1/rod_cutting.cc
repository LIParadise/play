/************************************************************************
 * Synopsis: Implementation of the O(n^2) time algorithm for rod cutting
 *
 * Author:   LIParadise
 *
 * Date:     2020 Nov. 11
 ************************************************************************/

#include <string>
#include <iostream>
#include <ios>
#include <algorithm>
#include <vector>
#include <iomanip>

using namespace std;

// both return array, say "ret", and "how_to" would have size (price_list.size()+1)
// ret.at(i) is the best possible price for rod of which length is i
// (how_to(i)+1) is the length where you should start cutting if your whole rod is "i" long.
// see "print_how_to" for more details of how to use "how_to"
vector<unsigned> cut_rod( const vector<unsigned>& price_list, unsigned len, vector<unsigned>& how_to ){
    // index u is BEST price of (len==u)
    vector<unsigned> best_prices;
    best_prices.reserve(len + 1);
    best_prices.push_back(0);
    how_to.reserve(len + 1);
    how_to.clear();
    how_to.push_back(0);

    for( unsigned u = 1; u <= price_list.size(); ++u ){
        // calculate best price for (len==u)
        unsigned max_price = 0, max_idx;
        // try use price of (len == (v+1)) directly from "price_list",
        // together with ( len == (u-(v+1)) ) from "best_prices"
        for( unsigned v = u-1; v < u; --v ){
            unsigned tmp_max = price_list.at(v) + best_prices.at(u-(v+1));
            if (tmp_max > max_price) {
                max_idx = v;
                max_price = tmp_max;
            }
        }
        best_prices.push_back(max_price);
        how_to.push_back(max_idx);
    }

    return best_prices;
}

void print_vector_beautify(const vector<unsigned> &, unsigned);
void print_how_to(const vector<unsigned> &, unsigned);

int main(){
    unsigned price;
    unsigned l;
    vector<unsigned> price_list, how_to;
    cout << "prices?" << endl;
    while( (cin >> price) && price != 0 ){
        price_list.push_back(price);
    }
    cout << "how long?" << endl;
    cin >> l;
    const unsigned len = l;

    cout << "Price List Is:" << endl;
    print_vector_beautify(price_list, 1);
    const auto& result = cut_rod(price_list, len, how_to);

    cout << endl << "How to cut input len " << len << "is: " << endl;
    print_how_to(how_to, len);
    cout << endl << "Best Price is " << result.at(len) << endl;
    cout << "All prices is " << endl;
    print_vector_beautify( result, 0 );
    cout << endl;

    return 0;
}

// print index first, then print content
// index is offset by "offset"
// e.g. print prices:
//      since prices start from 1, use "offset == 1"
// e.g. print how to cut:
//      My how_to start from length = 0, so use "offset == 0"
void print_vector_beautify(const vector<unsigned> &vec, unsigned offset) {
    ios_base::fmtflags cout_flags(cout.flags());
    for (unsigned u = 0; u < vec.size(); ++u) {
        cout << right << setw(6) << u + offset;
    }
    cout << endl;
    for (unsigned u = 0; u < vec.size(); ++u) {
        cout << right << setw(6) << vec.at(u);
    }
    cout.flags(cout_flags);
}

// "how_to.at(i)+1" is the length which you should cut,
// in order to maximize the profit of rod of which length is "i".
void print_how_to( const vector<unsigned> & how_to, unsigned len){
    ios_base::fmtflags cout_flags(cout.flags());
    while( len <= how_to.size() && len > 0 ){
        cout << "cut len" << right << setw(6) << how_to.at( len )+1 << endl;
        len = len - (how_to.at( len )+1);
    }
    cout.flags(cout_flags);
}
