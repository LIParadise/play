/************************************************************************
 * Synopsis: Implementation of the O(m) space, O(mn) time algorithm
 *           on finding the Longest Common Sequence (LCS) of 2 strings,
 *           of which have length m and n, respectively
 *
 * Author:   LIParadise
 *
 * Date:     2020 Nov. 10
************************************************************************/

#include <string>
#include <iostream>
#include <algorithm>
#include <cctype>
#include <vector>

using namespace std;

void to_lower( string& s ){
    transform(s.begin(), s.end(), s.begin(),
        [](unsigned char c){
            return std::tolower(c);
        }
    );
}

unsigned lcs_len( const string& s1, const string& s2){
    static vector<unsigned> vec;
    const string *p_Longer_str, *p_Shorter_str;
    unsigned left_up_backup;
    if( s1.size() < s2.size() ){
        p_Shorter_str = &s1;
        p_Longer_str = &s2;
    }else{
        p_Shorter_str = &s2;
        p_Longer_str = &s1;
    }
    vec.resize(p_Shorter_str->size() + 1);
    fill( vec.begin(), vec.end(), 0 );

    for (unsigned U = 0; U < p_Longer_str->size(); ++U) {
        left_up_backup = 0;
        for (unsigned u = 1; u < p_Shorter_str->size()+1; ++u) {
            if (p_Shorter_str->at(u - 1) == p_Longer_str->at(U)) {
                swap( left_up_backup, vec.at(u) );
                ++vec.at(u);
            } else {
                left_up_backup = vec.at(u);
                vec.at(u) = max( vec.at(u-1), vec.at(u) );
            }
        }
    }

    return vec.back();
}

int main(){
    string s1, s2;
    cout << "String 1?" << endl;
    cin >> s1;
    cout << "String 2?" << endl;
    cin >> s2;
    to_lower(s1);
    to_lower(s2);

    cout << "String 1 is \"" << s1 << "\"\nString 2 is \"" << s2 << "\"" << endl;

    cout << "len of LCS is " << lcs_len( s1, s2 ) << endl;
    return 0;
}
